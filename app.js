const express = require("express");
const multer = require("multer");
const ejs = require("ejs");
const path = require("path")

//Set Storage Engine
const storage = multer.diskStorage({
    destination: './public/uploads',
    filename: function(req, file, callback){
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

//Init Upload

const upload = multer({
    storage: storage, 
    limits: {fileSize: 10000000}, 
    fileFilter: function(req, file, callback){
        checkFileType(file, callback);
    }
}).single('myImage');


//Check File Type

function checkFileType(file, callback){
    //Allowed extensions
    const fileTypes = /jpeg|jpg|png|gif/;
    //Check Ext
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
    //Check mime
    const mimeType = fileTypes.test(file.mimetype)
    if(mimeType && extName){
        return callback(null, true);
    } else {
        callback("Error: Images Only")
    }
}

//Init app
const app = express();

//EJS (Templating)

app.set("view engine", "ejs");

//Public Folder

app.use(express.static("./public"));

app.get('/', (req, res) => res.render('index'));

app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if(err){
            res.render("index", {
                msg: err 
            });
            } else {
                if(req.file == undefined){
                    res.render("index", {
                        msg: "Error: No File Selected!"
                });
            } else {
                res.render("index", {
                    msg: "File Uploaded!", 
                    file: `uploads/${req.file.filename}`
                });
        }
    }
    });
});

const port = 3000;

app.listen(port, () => console.log (`Server started on port ${port}`));